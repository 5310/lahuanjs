var Lahuan = Lahuan || Object.create(null);

(function (Lahuan) {

	/**
	 * The Hook module implements basic arbitrary method string-identified contextual "hooks" with customizable scope and optional an payload.
	 * Yes, events, basically, pub/sub.
	 *
	 * Contains a hook-manager constructor that can be instanced and used as-is, as a property, to access above-mentioned functionality.
	 *
	 * Also exposes a globally accessible hook-manager for drop-in use.
	 *
	 * @namespace
	 * @version			0.1.2
	 */
	Lahuan.hook = Object.create(null);

	/**
	 * Creates a hook-manager that other methods can bind to and trigger hooks on.
	 *
	 * The instantiated hook-manager can function stand-alone, as a property, or can be mixed-in.
	 *
	 * @constructor
	 */
	Lahuan.hook.HookManager = function HookManager() {

		/**
		 * @callback	Lahuan.hook~hookCallback
		 * @arg			{Object}		[payload=undefined] - Contains any arbitrary data passed on by the triggering hook.
		 */

		/**
		 * Contains all hooks that are bound to this instance of hook-manager mapped to a list of all callbacks bound to that hook.
		 * @member		{Object}
		 * @private
		 */
		this._hooks = {};

	};

	/**
	 * Binds an arbitrary callback to an arbitrary hook event on this hook-manager instance.
	 * @arg			{String}		hook - The hook to bind to.
	 * @arg			{Lahuan.hook~hookCallback}	callback - The callback function that will trigger with the hook.
	 * @returns		{Object}		Returns itself for chainability.
	 */
	Lahuan.hook.HookManager.prototype.bind = function (hook, callback) {
		this._hooks[hook] = this._hooks[hook] || [];
		this._hooks[hook].push(callback);
		return this;
	};

	/**
	 * Unbinds a previously bound callback from the hook-manager instance given the hook.
	 * @arg			{String}		hook - The hook to unbind from.
	 * @arg			{Lahuan.hook~hookCallback}	callback - The callback function that will be removed from the hook.
	 * @returns		{Object}		Returns itself for chainability.
	 */
	Lahuan.hook.HookManager.prototype.unbind = function (hook, callback) {
		var callbacks = this._hooks[hook];
		for (var i in callbacks) {
			if (callback == callbacks[i]) {
				callbacks.splice(i, 1);
			}
		}
		return this;
	};

	/**
	 * Triggers an arbitrary hook on the hook-manager instance.
	 * @arg			{String}		hook - The hook to trigger.
	 * @arg			{Object}		[payload=undefined] - The payload object which will be passed to the callback function. Defaults to `undefined`.
	 * @arg			{Object}		[scope=this] - The scope under which the callback will be called.
	 * @returns		{Object}		Returns itself for chainability.
	 */
	Lahuan.hook.HookManager.prototype.trigger = function (hook, payload, scope) {
		scope = scope || this;
		var callbacks = this._hooks[hook];
		for (var i in callbacks) {
			callbacks[i].call(scope, payload);
		}
		return this;
	};



	/**
	 * The hook-manager instance that is further exposed through the namespace for drop-in use.
	 * @member		{Lahuan.hook.HookManager}
	 * @private
	 */
	Lahuan.hook._globalHookManager = new Lahuan.hook.HookManager();
	/**
	 * Binds an arbitrary callback to an arbitrary hook event on this globally accessible hook-manager instance.
	 * @function
	 * @arg			{String}		hook - The hook to bind to.
	 * @arg			{Lahuan.hook~hookCallback}	callback - The callback function that will trigger with the hook.
	 * @returns		{Object}		Returns itself (and not the private hook-manager instance) for chainability.
	 */
	Lahuan.hook.bind = function (hook, callback) {
		Lahuan.hook._globalHookManager.bind(hook, callback);
		return this;
	};
	/**
	 * Unbinds a previously bound callback from this globally accessible hook-manager instance.
	 * @function
	 * @arg			{String}		hook - The hook to unbind from.
	 * @arg			{Lahuan.hook~hookCallback}	callback - The callback function that will be removed from the hook.
	 * @returns		{Object}		Returns itself (and not the private hook-manager instance) for chainability.
	 */
	Lahuan.hook.unbind = function (hook, callback) {
		Lahuan.hook._globalHookManager.unbind(hook, callback);
		return this;
	};
	/**
	 * Triggers an arbitrary hook on this globally accessible hook-manager instance.
	 * @function
	 * @arg			{String}		hook - The hook to trigger.
	 * @arg			{Object}		[payload=undefined] - The payload object which will be passed to the callback function. Defaults to `undefined`.
	 * @arg			{Object}		[scope=this] - The scope under which the callback will be called.
	 * @returns		{Object}		Returns itself (and not the private hook-manager instance) for chainability.
	 */
	Lahuan.hook.trigger = function (hook, payload, scope) {
		Lahuan.hook._globalHookManager.bind(hook, payload, scope);
		return this;
	};

}(Lahuan));
