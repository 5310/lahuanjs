var Lahuan = Lahuan || Object.create(null);

(function (Lahuan) {

	/**
	 * Contains a loose collection of utility functions.
	 *
	 * @namespace
	 * @version			0.1.0
	 */
	Lahuan.util = Object.create(null);

	/**
	 * Clones a Javascript object, or returns any literal as is.
	 *
	 * Here's what "cloning" means in this case, as this is case rife with ambiguity:
	 * 1. If `object` is not really an object, return as is.
	 * 2. If it _is_ an object, create a new object with its prototype as its prototype.
	 * 3. Assign all own properties of the original object to the cloned object directly.
	 *
	 * @arg			{Object}	object - The object to clone.
	 * @arg			{Object}	descriptorFlags - Contains flags that decide whether specific Javascript descriptors are retained for all cloned properties.
	 *				More information: [`Object.defineProperty`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty);
	 * @arg			{boolean}	[descriptorFlags.configurable=false] - If true all properties' configurable descriptors are retained for this clone.
	 * @arg			{boolean}	[descriptorFlags.enumerable=false] - If true all properties' enumerable descriptors are retained for this clone.
	 * @arg			{boolean}	[descriptorFlags.value=true] - If true all properties' values are retained for this clone, or else set to undefined.
	 * @arg			{boolean}	[descriptorFlags.writable=false] - If true all properties' writable descriptors are retained for this clone.
	 * @arg			{boolean}	[descriptorFlags.get=false] - If true all properties' getter functions are retained for this clone.
	 * @arg			{boolean}	[descriptorFlags.set=false] - If true all properties' setters functions are retained for this clone.
	 *
	 * @returns		The clone.
	 */
	Lahuan.util.clone = function (object, descriptorFlags) {

		if (object && typeof object === "object") {

			var clone = Object.create(Object.getPrototypeOf(object));

			var properties = Object.getOwnPropertyNames(object);
			for (var i in properties) {
				var property = properties[i];
				if (descriptorFlags) {

					var descriptor = Object.getOwnPropertyDescriptor(object, property);

					descriptor.configurable = descriptorFlags.configurable ? descriptor.configurable : false;
					descriptor.enumerable = descriptorFlags.enumerable ? descriptor.enumerable : false;

					// Property descriptors are either of the data-descriptor or the accessor-descriptor variety.
					if ( descriptor.value ) {
						// Value should be retained by default.
						descriptor.value = descriptorFlags.value || descriptorFlags.value === undefined ? descriptor.value : undefined;
						descriptor.writable = descriptorFlags.writable ? descriptor.writable : false;
					} else {
						descriptor.get = descriptorFlags.get ? descriptor.get : undefined;
						descriptor.set = descriptorFlags.set ? descriptor.set : undefined;
					}

					Object.defineProperty(clone, property, descriptor);

				} else {
					clone[property] = object[property];
				}
			}

			return clone;

		} else {
			return object;
		}

	};

	/**
	 * Recursively clones a Javascript object, or returns any literal as is.
	 *
	 * Here's what "recursively cloning" means in this case, as this is case rife with ambiguity:
	 * 1. If `object` is not really an object, return as is.
	 * 2. If it _is_ an object, create a new object with its prototype as its prototype.
	 * 3. Recursively `cloneDeep` all own properties of the original object to the cloned object.
	 *
	 * @arg			object - The object to recursively clone.
	 * @arg			{Object}	descriptorFlags - Contains flags that decide whether specific Javascript descriptors are retained for all recursively cloned properties.
	 *				More information: [`Object.defineProperty`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty);
	 * @arg			{boolean}	[descriptorFlags.configurable=false] - If true all properties' configurable descriptors are retained for this deep clone.
	 * @arg			{boolean}	[descriptorFlags.enumerable=false] - If true all properties' enumerable descriptors are retained for this deep clone.
	 * @arg			{boolean}	[descriptorFlags.value=true] - If true all properties' values are again cloned recursively to this deep clone
	 *				with the same descriptor retention flags,
	 *				or set to undefined for this deep clone.
	 * @arg			{boolean}	[descriptorFlags.writable=false] - If true all properties' writable descriptors are retained for this deep clone.
	 * @arg			{boolean}	[descriptorFlags.get=false] - If true all properties' getter functions are retained for this deep clone.
	 * @arg			{boolean}	[descriptorFlags.set=false] - If true all properties' setters functions are retained for this deep clone.
	 *
	 * @returns		The recursive clone.
	 */
	Lahuan.util.cloneDeep = function (object, descriptorFlags) {

		if (object && typeof object === "object") {

			var clone = Object.create(Object.getPrototypeOf(object));

			var properties = Object.getOwnPropertyNames(object);
			for (var i in properties) {
				var property = properties[i];
				if (descriptorFlags) {

					var descriptor = Object.getOwnPropertyDescriptor(object, property);

					descriptor.configurable = descriptorFlags.configurable ? descriptor.configurable : false;
					descriptor.enumerable = descriptorFlags.enumerable ? descriptor.enumerable : false;

					// Property descriptors are either of the data-descriptor or the accessor-descriptor variety.
					if ( descriptor.value ) {
						// Value should be retained by default.
						descriptor.value = descriptorFlags.value || descriptorFlags.value === undefined ? Lahuan.util.cloneDeep(descriptor.value) : undefined;
						descriptor.writable = descriptorFlags.writable ? descriptor.writable : false;
					} else {
						descriptor.get = descriptorFlags.get ? descriptor.get : undefined;
						descriptor.set = descriptorFlags.set ? descriptor.set : undefined;
					}

					Object.defineProperty(clone, property, descriptor);

				} else {
					clone[property] = Lahuan.util.cloneDeep(object[property]);
				}
			}

			return clone;

		} else {
			return object;
		}

	};

	/**
	 * Mixes in a Javascript object into a another object. Own-properties of the source object is added to the destination object.
	 *
	 * Since only own properties are mixed in, and the function doesn't even look at any prototypal properties, this is similar to most mixin implementations out there,
	 * save for the optional property descriptor retention.
	 *
	 * @arg			{Object}	source - The source object to mix own-properties from.
	 * @arg			{Object}	destination - The destiantion object to mix those own-properties into.
	 * @arg			{Object}	descriptorFlags - Contains flags that decide whether specific Javascript descriptors are retained for all mixed in own-properties.
	 *				More information: [`Object.defineProperty`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty);
	 * @arg			{boolean}	[descriptorFlags.configurable=false] - If true all source own-properties' configurable descriptors are retained on destination.
	 * @arg			{boolean}	[descriptorFlags.enumerable=false] - If true all source own-properties' enumerable descriptors are retained on destination.
	 * @arg			{boolean}	[descriptorFlags.value=true] - If true all source own-properties' values are retained on destination, or else set to undefined.
	 * @arg			{boolean}	[descriptorFlags.writable=false] - If true all source own-properties' writable descriptors are retained on destination.
	 * @arg			{boolean}	[descriptorFlags.get=false] - If true all source own-properties' getter functions are retained on destination.
	 * @arg			{boolean}	[descriptorFlags.set=false] - If true all source own-properties' setters functions are retained on destination.
	 *
	 * @returns		The clone.
	 */
	Lahuan.util.mixin = function (source, destination, descriptorFlags) {

		var properties = Object.getOwnPropertyNames(source);
		for (var i in properties) {
			var property = properties[i];
			if (descriptorFlags) {

				var descriptor = Object.getOwnPropertyDescriptor(source, property);

				descriptor.configurable = descriptorFlags.configurable ? descriptor.configurable : false;
				descriptor.enumerable = descriptorFlags.enumerable ? descriptor.enumerable : false;

				// Property descriptors are either of the data-descriptor or the accessor-descriptor variety.
				if ( descriptor.value ) {
					// Value should be retained by default.
					descriptor.value = descriptorFlags.value || descriptorFlags.value === undefined ? descriptor.value : undefined;
					descriptor.writable = descriptorFlags.writable ? descriptor.writable : false;
				} else {
					descriptor.get = descriptorFlags.get ? descriptor.get : undefined;
					descriptor.set = descriptorFlags.set ? descriptor.set : undefined;
				}

				Object.defineProperty(destination, property, descriptor);

			} else {
				destination[property] = source[property];
			}
		}

		return destination;

	};

	/**
	 * Checks if the given string is a valid Javascript variable name. Courtesy of:
	 *
	 * Does _not_ check for reserved keywords.
	 *
	 * Courtesy of [bobince's answer](http://stackoverflow.com/questions/2008279/validate-a-javascript-function-name/2008444#2008444)
	 * on [this Stack Overflow question](http://stackoverflow.com/questions/2008279/validate-a-javascript-function-name).
	 *
	 * @arg			{string}	name - The string to check for validity as a variable name.
	 */
	Lahuan.util.isValidName = function (name) {
		return !/^[_$a-zA-Z\xA0-\uFFFF][_$a-zA-Z0-9\xA0-\uFFFF]*$/i.test(name);
	};

}(Lahuan));
