var Lahuan = Lahuan || Object.create(null);

(function (Lahuan) {

	/**
	 * The Ent module implements an entity-component framework in a way _I_ find intuitive.
	 *
	 * - Entities are just an object than can aggregate components,
	 *   and components are just objects that get added to entities as unique properties.
	 * - A special objects called space contain entities and other nested spaces for organization,
	 *   indexing, searching, and processing. Also aggregates processors.
	 * - Processors (often called systems by people other than T=Machine :)
	 *   attach to systems like components to entities and batch-process entities inside them.
	 *
	 * It is worth noting that Ent only implements a bare-bones entity-component system.
	 * It doesn't implement any default spaces, processors, or even event-handling.
	 * The module Env implements a basic system with default space and processes for developing an interactive system over.
	 *
	 * Ent depends on `Lahuan.util` and `Lahuan.hook`.
	 *
	 * @namespace
	 * @version			0.1.0
	 */
	Lahuan.ent = Object.create(null);

	/**
	 * Creates a component _blueprint_ object that can be stored in a library and added to entities.
	 *
	 * In reality, this constructor just validates a standard component definiton object,
	 * or creates one from just a given id, and mixes it into the constructed object.
	 *
	 * The component definiton specification is very minimal, as described shortly below.
	 * The definiton can contain any arbitrary own-properties in addition to the standard ones,
	 * and they'll be retained in the component blueprint/object.
	 * Naturally, these own-properties will define the actual functionality/data-storage of the component being defined.
	 *
	 * @constructor
	 *
	 * @arg			{Lahuan.ent.Component~ComponentDefinition|string}		definition - Either a standard definiton or a string containing a component id.
	 *
	 * @throws		"InvalidComponentDefiniton" when supplied definition object is broken.
	 * @throws		"InvalidComponentDependencies" when supplied dependencies list is not valid.
	 * @throws		"InvalidComponentID" when given component ID is not a valid name.
	 * @throws		"InvalidComponentInitCallback" when given initialization callback is not a valid function.
	 * @throws		"InvalidComponentDestroyCallback" when given destruction callback is not a valid function.
	 */
	Lahuan.ent.Component = function (definition) {

		// Validate the component definiton.

		var component;

		var type = typeof definition;
		if (type === "object") {
			component = definition;
		} else if (type === "string") {
			component = {
				id: definition
			};
		} else {
			throw "InvalidComponentDefiniton";
		}

		var isValidName = function (name) {
			return !/^[_$a-zA-Z\xA0-\uFFFF][_$a-zA-Z0-9\xA0-\uFFFF]*$/i.test(name);
		};

		if (isValidName(component.id)) {
			throw "InvalidComponentID";
		}
		if (component.dependencies !== undefined || component.dependencies !== null) {
			if (component.dependencies instanceof Array) {
				for (var i in component.dependencies) {
					var dependency = component.dependencies[i];
					var type = typeof dependency;
					var id;
					if (type === "object") {
						id = dependency.id;
					} else if (type === "string") {
						id = dependency;
					}
					if (isValidName(id)) {
						throw "InvalidComponentDependencies";
					}
				}
			} else {
				throw "InvalidComponentDependencies";
			}
		}
		if (component.init && typeof component.init !== "function") {
			throw "InvalidComponentInitCallback";
		}
		if (component.init && typeof component.destroy !== "function") {
			throw "InvalidComponentDestroyCallback";
		}

		// Simply mix in all own-properties into the constructed object.

		var properties = Object.getOwnPropertyNames(component);
		for (var i in properties) {
			var property = properties[i];
			this[property] = component[property];
		}

	};

	/**
	 * The component definition object implements all the functionality a component is to provide.
	 *
	 * The [component constructor]{@link Lahuan.ent.Component} takes definitons objects to produce blueprints,
	 * which can then be cloned and added to entities.
	 * While it is possible to add a component definition object to an entity directly, it is discouraged,
	 * as the component constructor does validation over the definition.
	 *
	 * In practice, these definitions objects can be anonymously supplied to the component constructor
	 * and only the constructed blueprint object be stored for further use.
	 *
	 * Any property other than the ones described below and `entity` will be added to the every instantiated component object,
	 * and indeed is to be used to implement most if not all of the component's functionality of what there is to be.
	 *
	 * The property `entity` will be available in an instantiated component object part of an entity containing a reference to it,
	 * for use with any functionality implemented within the component that needs it.
	 *
	 * @example
	 * // Assuming a component called `2D` is already defined as expected.
	 * var Offset10 = Lahuan.ent.Component({
	 *     id: "Offset10",
	 *     dependencies: ["2D"],
	 *     init: function() {
	 *         this.entity.2D.x += this.offsent;
	 *         this.entity.2D.y += this.offsent;
	 *     },
	 *     destroy: function() {
	 *         this.entity.2D.x -= this.offsent;
	 *         this.entity.2D.y -= this.offsent;
	 *     },
	 *     offset: 10
	 * });
	 *
	 * @typedef		{Object}	Lahuan.ent.Component~ComponentDefinition
	 * @property	{string}	id - The id of the defined component. Must be unique, lest havok is wrecked.
	 * @property	{Array}		[dependencies] - A list components that must already exist in an entity for the component to be succesfully added.
	 *				Can be a string with the component's id, or a component clueprint instance itself, or even an arbitrary component-like object with an id.
	 *				But keep in mind, when you're constructing component blueprints, references to dependent components may or may not be defined yet.
	 * @property	{Lahuan.ent.Component~componentInitCallback}	[init] - The initialization callback that is ran when a component object
	 *				is instantiated and added to an entity. Perform all set-up functionality for a new component here.
	 * @property	{Lahuan.ent.Component~componentDestroyCallback}	[destroy] - The destruction callback that is ran when the component is destroyed.
	 *				Perform all clean-up activity here.
	 */
	/**
	 * This callback is ran atuomatically when a component object is instantiated and added to an entity.
	 * @callback	Lahuan.ent.Component~componentInitCallback
	 */
	/**
	 * This callback is ran when a component object is removed from an entity or when the entity is destroyed.
	 * @callback	Lahuan.ent.Component~componentDestroyCallback
	 */

	Lahuan.ent.Entity = function (space) {

		this.id = "UUID";

		this.spaces = [];

		this.addComponent = function () {};

		this.removeComponent = function () {};

		this.hasComponent = function () {};

		this.checkComponentDependency = function () {};

		this.then = function (callback) {};

		this.set = function () {};

		this.get = function () {};

		this.destroy = function () {};

	};

}(Lahuan, Lahuan.util, Lahuan.hook));
