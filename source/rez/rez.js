var Lahuan = Lahuan || Object.create(null);

(function (Lahuan, Resurrect) {

	/**
	 * //TODO: TBD
	 * @namespace
	 * @version			0.1.0
	 */
	Lahuan.rez = Object.create(null);

	/**
	 * Resolves prototypes of constructors by searching in the global namespace first,
	 * and then on the properties of the given "namespace" object,
	 * and finally, recursively within the given "namespace" object.
	 *
	 * @constructor
	 * @arg		{Object}	scope - Any object to be searched as a namespace for constructors.
	 */
	Resurrect.RecursiveNamespaceResolver = function(scope) {
		this.scope = scope;
		this.cache;
		this.recursionLimit = 10000;
		this.keepCache = true;
		this.shadowProperties = true;
	};


	/**
	 * Traverse the scope recursively and index all the functions and potential constructors within.
	 *
	 * If `this.shadowProperties` is true, then properties with the same name that are nested deeper overwrite shallower properties on the cache.
	 * Recursion is limited by the property `this.recursionLimit`.
	 *
	 * @constructor
	 * @arg		{Object}	scope - Any object to be searched as a namespace for constructors.
	 */
	Resurrect.RecursiveNamespaceResolver.prototype.updateCache = function() {
		var _this = this;
		this.cache = Object.create(null);
		var recursionLimit = this.recursionLimit;
		var f = function(object) {
			if (recursionLimit-- < 0) {
				return;
			}
			var ownPropertyNames = Object.getOwnPropertyNames(object);
			for ( var i in ownPropertyNames ) {
				var propertyName = ownPropertyNames[i];
				var property = object[propertyName];
				if (typeof property === "function") {
					if (!_this.cache[propertyName]) {
						_this.cache[propertyName] = property;
					} else {
						if (_this.shadowProperties) {
							_this.cache[propertyName] = property;
							console.warn("A function by the name of "+propertyName+" has been shadowed in this cache by a deeper function.");
						} else {
							console.warn("A function by the name of "+propertyName+" already exists in this cache and has not been shadowed.");
						}
					}
				} else if (typeof property === "object" && !(property instanceof Array)) {
					f(property);
				}
			}
		};
		f(this.scope);
	}

	/**
	 * Gets the prototype (constructor function) of the given property name.
	 *
	 * If `this.keepCache` is set to false every time this function is called the cache will be regenerated.
	 *
	 * First searches within global scope and then, if necessary, within the scope by proxy of the flattened cache.
	 *
	 * If not found, it throws an error.
	 * If recursively searching for too long, throws an error.
	 *
	 * @arg 	{string} 	name - Name of the property (constructor function) to check for.
	 * @arg 	{string} 	[recursionLimit=10000] - Optional maximum recursive search limit.
	 *
	 * @returns				The prototype of the property (constructor function) if found.
	 */
	Resurrect.RecursiveNamespaceResolver.prototype.getPrototype = function(name, recursionLimit) {

		var constructor;

		// See if prototype exists globally.
		constructor = Resurrect.GLOBAL[name];
		// And then check inside the scope if necessary.
		if (!constructor || this.shadowProperties) {
			// Update cache from scope if necessary.
			if (!this.cache || !this.keepCache) {
				this.updateCache();
			}
			constructor = this.cache[name];
		}

		if (constructor) {
			return constructor.prototype;
		} else {
			throw new Resurrect.prototype.Error('Unknown constructor: ' + name);
		}

	};

	/**
	 * Get the prototype name for an object, to be fetched later with getPrototype.
	 *
	 * Same as `Resurrect.NamespaceResolver.prototype.getName`.
	 *
	 * @arg 		{Object} 	object - The object to fetch constructor name of.
	 *
	 * @returns 	{string} 	Null if the constructor is Object.
	 *
	 * @method
	 */
	Resurrect.RecursiveNamespaceResolver.prototype.getName = Resurrect.NamespaceResolver.prototype.getName;

	Lahuan.rez._resolver;
	Lahuan.rez._necromancer;
	Lahuan.rez._registry = Object.create(null);
	Lahuan.rez.register = function(name, f) {
		if(Lahuan.rez.registry[name]) {
			console.warn("Function by the name of "+name+" already exists in the registry and will not be overwritten.");
		} else {
			if (typeof f !== "function") {
				throw new TypeError("object to be registered is not a function.");
			} else {
				Lahuan.rez._registry[name] = f;
			}
		}
	};
	Lahuan.rez.deregister = function(property) {
		delete Lahuan.rez.registry[name];
	}
	Lahuan.rez.stringify;
	Lahuan.rez.resurrect;

}(Lahuan, Resurrect));
