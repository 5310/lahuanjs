exports.defineTags = function(dictionary) {

	/**
	* The Lahuan Hook module does not implement anything new in JSDoc,
	* but it does use the following two synonyms.
	*/
	dictionary.lookUp("event").synonym("hook");
	dictionary.lookUp("fires").synonym("triggers");

};
