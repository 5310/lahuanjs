Lahuan.js
=========

Lahuan is a library / utility-belt started for personal use providing a few mostly high-level game-dev related functionality.

Lahuan should contain many different modules, most stand-alone, some interdependent, offering particular functionality that I often find myself reaching for. Some may seem redundant, such as the entity system that I wrote _just because_ none of the existing ones fit my personal tastes. There are some more relatively specific ones such the messaging module that implements a persistent medium based messaging system. Some may be far more specific, such the natural-language generation toolkit I use for defining vocabulary and templates, etc.

Usage
-----

I don't expect Lahuan.js to be usable by anyone else _anytime soon,_ although I plan on keeping things as clean and well-documented as possible. Lahuan is probably too personal to be useful to that many people, but I do plan on releasing it eventually.

As already mentioned, Lahuan is split into several modules. I will be implementing these as I need them (or more practically, shortly after having re/used them and after having cleaned them up). These modules should be available as single-file imports so projects can pick and choose which to include.

Currently, I'm encapsulating everything using closured namespace extensions. (Essential JS Design patterns are incredibly cumbersome to document using JSDoc, and this is just as effective anyway.) Therefore, this should be usable in any Javascript project (barring namespace conflicts within Lahuan.js itself and dependencies caused by the project). I plan tomove to [ES6 modules][ES6-modules] when they arrive for good.

Lahuan.js modules are bound the have a few external dependencies based on their use. Already, I feel [Lo-Dash] to be completely indispensable, for example. Lahuan.js modules will just expect these dependencies to exist in whatever context they're used, as is usual.

Repository Structure
--------------------

I've decided to organize this repository as follows:

-   `./`
    -   `build/`
        -   Contains all the concatenated and minified module scripts for use in projects.
        -   Contains a single global minified/concatenated script containing all the modules.
        -   Until the process is automated, this folder will likely remain empty.
    -   `docs/`
        -   Will contain all the generated docs generated from the source JSDoc3 comments.
        -   Until the process is automated, this folder will likely remain empty.
        -   Ideally, this folder will be removed.
            -   All generated html documentation will go to the Github page, if at all.
            -   And maybe the documentation will be added to the Github Wiki automatically through JsDox etc.
    -   `external/`
        -   External dependencies such as crucial libraries that modules use.
        -   Ideally, everything in this folder will be a Git submodule to the source project.
        -   Or a simple folder containing the dependency updated manually.
        -   Only test environments and cases import these dependencies, like most Js dependant libraries, Lahuanjs will assume they exist.
    -   `scratch/`
        -   A folder for personal use.
        -   Not tracked by Git.
    -   `source/`
        -   Contains the actual source files.
        -   If it exists, the core Lahuan library will exist a single file here.
        -   And all modules will exist as a folder of their name.
            -   Which will be populated with one or more source files implementing that module.
    -   `tests/`
        -   Contains test-environments which imports all dependencies and lets me poke around in the console or run test-scripts manually.
        -   Until actual test-cases are implemented, this folder will remain untracked.
	-	`jsdoc/`
		-	Contains a manual JSDoc building shell-script, a local config file, and eventually, plugins extending definitions related to Lahuan.
	-	`gh-pages/`
		-	Contains the gh-pages branch as a submodule. handy, eh?
		-	`docs`
			-	The JSDoc output of source.
    -   `readme.md`
        -   This file.
    -   `todo.md`
        -   A hierarchical todo-list tracking planned and completed tasks for the root project.
        
        
[module-patterns]: http://addyosmani.com/resources/essentialjsdesignpatterns/book/#modulepatternjavascript
[namespacing-patterns]: http://addyosmani.com/resources/essentialjsdesignpatterns/book/#detailnamespacing
[ES6-modules]: http://wiki.ecmascript.org/doku.php?id=harmony:modules
[Lo-Dash]: http://lodash.com/
